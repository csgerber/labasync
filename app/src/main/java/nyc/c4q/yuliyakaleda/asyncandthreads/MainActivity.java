package nyc.c4q.yuliyakaleda.asyncandthreads;


import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;


public class MainActivity extends ActionBarActivity {

    private static final String url = "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1";
    private static final String TAG_ITEMS = "items";
    private static final String TAG_MEDIA = "media";
    private static final String TAG_M = "m";
    private static final int MAX_RESULTS = 5;

    private ListView lv;
    private JSONArray items;
   // private GetPictures myAsyncTask;

    private ProgressBar progressBar;
    private Button buttonReload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonReload = (Button) findViewById(R.id.reload);
        buttonReload.setText("Start");
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        buttonReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                myAsyncTask = new GetPictures();
//                myAsyncTask.execute();
               // new ProgressTask().execute(100);

                new SetProgressTask().execute(100);
            }
        });
    }

    class SetProgressTask extends AsyncTask<Integer, Integer, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            buttonReload.setText("LOADING...");

        }



        @Override
        protected String doInBackground(Integer... strings) {



            for (int nC = 0; nC < strings[0]; nC++) {
                try {
                    Thread.sleep(20);



                    publishProgress(nC);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            return "Done";



        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
            buttonReload.setText(String.valueOf(values[0]));
        }




        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            buttonReload.setText("Start");
            Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
        }




    }




//    class ProgressTask extends AsyncTask<Integer, Integer, String> {
//
//
//        @Override
//        protected void onPreExecute() {
//            buttonReload.setText("Loading...");
//
//        }
//
//
//        @Override
//        protected String doInBackground(Integer... params) {
//            for (int count = 0; count <= params[0]; count++) {
//                try {
//                    Thread.sleep(1);
//                    publishProgress(count);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            return "Task Completed.";
//        }
//
//        @Override
//        protected void onProgressUpdate(Integer... values) {
//            progressBar.setProgress(values[0]);
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//            progressBar.setProgress(100);
//            buttonReload.setText("Start");
//        }


























//    class GetPictures extends AsyncTask<Void, Void, ArrayList<String>> {
//        @Override
//        protected ArrayList<String> doInBackground(Void... voids) {
//            ArrayList<String> arrayImages = new ArrayList<String>();
//            BufferedReader reader;
//            String line;
//
//            try {
//                URL urlString = new URL(url);
//                HttpURLConnection connection = (HttpURLConnection) urlString.openConnection();
//                StringBuilder stringBuilder = new StringBuilder();
//                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//
//                while ((line = reader.readLine()) != null) {
//                    stringBuilder.append(line + "\n");
//                }
//                String resultString = stringBuilder.toString();
//
//                if (resultString != null) {
//                    try {
//                        JSONObject jsonObj = new JSONObject(resultString);
//                        items = jsonObj.getJSONArray(TAG_ITEMS);
//                        for (int i = 0; i < MAX_RESULTS; i++) {
//                            JSONObject c = items.getJSONObject(i);
//                            JSONObject obj = c.getJSONObject(TAG_MEDIA);
//                            String m = obj.getString(TAG_M);
//                            arrayImages.add(m);
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//                //Log.e("MainActivity", "Caught  while ... ", e);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return arrayImages;
//        }
//
//
//        @Override
//        protected void onPostExecute(ArrayList<String> imagesArray) {
//            lv = (ListView) findViewById(R.id.lv);
//            ImageAdapter adapter = new ImageAdapter(MainActivity.this, imagesArray);
//            lv.setAdapter(adapter);
//        }
//    }
}


